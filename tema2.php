<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php
            print "Version PHP ID: ".PHP_RELEASE_VERSION;
            echo '<br>';
            print "ID PHP: ".PHP_VERSION_ID;
            echo '<br>';
            print "Valor maximo de enteros: ".PHP_INT_MAX;
            echo '<br>';
            print "Tamaño máximo del nombre de un archivo: ".PHP_MAXPATHLEN;
            echo '<br>';
            print "Versión del Sistema Operativo: ".PHP_OS;
            echo '<br>';
            print "Símbolo correcto de 'Fin De Línea' para la plataforma en uso: ".PHP_EOL;
            echo '<br>';
            print "Include Path: ".DEFAULT_INCLUDE_PATH;
        ?>
    </body>
</html>